(function ($) {

  var fetchTickerData = function(table) {
    // TODO: replace static url
    var nasdaqUrl = 'http://www.nasdaq.com/aspxcontent/NasdaqRSS.aspx?data=quotes';
    //$.get(nasdaqUrl + '&symbol=' + ticker, function( data ) {

     //});
  }

  Drupal.behaviors.monkeyTicker = {
    attach: function (context, settings) {
      fetchTickerData($('#ticker-grid', context));
      if(settings.monkeyWatchlists.autoupdate) {
        interval = setTimeout(
          fetchTickerData($('#ticker-grid', context)),
          parseInt(settings.monkeyWatchlists.autoupdateInterval) * 1000
        );
      }
    }
  };
})(jQuery);
