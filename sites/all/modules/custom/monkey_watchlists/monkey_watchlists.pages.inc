<?php

/**
 * Menu callback for user listing page.
 */
function monkey_watchlists_list_page() {
  global $user;

  $query = new EntityFieldQuery();
  $result = $query
  ->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'watchlist')
  ->propertyCondition('status', 1)
  ->propertyCondition('uid', $user->uid);

  $result = $query->execute();
  if(!empty($result)) {
    $nids = array_keys($result['node']);
    $nodes = node_load_multiple($nids);
    $output = node_view_multiple($nodes, 'teaser');
  }
  else {
    $output = t("You didn't create any watchlists yet, click !link to create your first!",
      array('!link' => l(t('here'), 'node/add/watchlist'))
    );
  }
  return $output;
}