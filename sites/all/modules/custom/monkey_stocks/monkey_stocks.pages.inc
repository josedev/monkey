<?php

/**
 * @file
 * Pages and forms for wishlists and stock creation and management.
 */

/**
 *
 * Form for wishlist creation.
 */
function monkey_stocks_wishlist_form ($form, &$form_state, $wishlist = FALSE) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => TRUE,
    '#default_value' => $wishlist ? $wishlist->title : '',
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#description' => t('A short pragraph describing this list.'),
    '#default_value' => $wishlist ? $wishlist->description : '',
  );

  $form['wid'] = array(
    '#type' => 'hidden',
    '#value' => $wishlist ? $wishlist->wid : 0,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );

  return $form;
}

/**
 *
 * Form for wishlist creation submission.
 */
function monkey_stocks_wishlist_form_validate ($form, &$form_state) {
  // Make sure the user didn't reach max limit for wishlists.
  global $user;
  if(!$form_state['values']['wid']) {
    if(count(monkey_stocks_get_user_wishlists($user->uid)) >= variable_get('monkey_stock_max_lists_user', 10)) {
      form_set_error('', t('You reached the maximim amount of wishlists allowed.'));
    }
  }
}

/**
 *
 * Form for wishlist creation submission.
 */
function monkey_stocks_wishlist_form_submit ($form, &$form_state) {
  global $user;
  $record = new stdClass();
  $record->title       = $form_state['values']['title'];
  $record->description = $form_state['values']['description'];
  $record->uid         = $user->uid;
  // Check if edit or new.
  if(!$form_state['values']['wid']) {
    drupal_write_record('wishlists', $record);
  }
  else {
    $record->wid = $form_state['values']['wid'];
    drupal_write_record('wishlists', $record, array('wid'));
  }
  $form_state['redirect'] = url('wishlist/' . $record->wid);
}

/**
 *
 * Form for wishlist delelte confirmation.
 */
function wishlist_delete_confirm ($form, &$form_state, $wishlist) {
  $form['#wishlist'] = $wishlist;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['wid'] = array('#type' => 'value', '#value' => $wishlist->wid);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $wishlist->title)),
    'wishlist/' . $wishlist->wid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Title callback for wishlist view page.
 */
function monkey_stocks_wishlist_page_title ($wishlist) {
  return $wishlist->title;
}

/**
 * Menu callback for wishlist view page.
 */
function monkey_stocks_wishlist_page_view($wishlist) {
  return 'kalb';
}