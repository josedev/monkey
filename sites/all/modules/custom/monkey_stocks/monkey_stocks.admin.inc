<?php

/**
 *
 * Callback for monkey stock admin form.
 */
function monkey_stocks_admin() {
  $form = array();

  $form['monkey_stock_max_lists_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed number of lists per user'),
    '#default_value' => variable_get('monkey_stock_max_lists_user', 10),
    '#size' => 60,
    '#maxlength' => 10,
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['monkey_stock_max_stocks_list'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed number of stock per wishlist'),
    '#default_value' => variable_get('monkey_stock_max_stocks_list', 10),
    '#size' => 60,
    '#maxlength' => 10,
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['auto_update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Auto-update settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['auto_update']['monkey_stock_auto_update'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('monkey_stock_auto_update', FALSE),
    '#title' => t('Enable autoupdate'),
  );

  $form['auto_update']['monkey_stock_auto_update_freq'] = array(
    '#title' => t('Autoupdate frequency'),
    '#type' => 'select',
    '#options' => array(0 => t('Never')) + drupal_map_assoc(array(30, 60, 120, 300), 'format_interval'),
    '#default_value' => variable_get('monkey_stock_auto_update_freq', 30),
  );

  return system_settings_form($form);
}